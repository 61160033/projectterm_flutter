import 'package:flutter/material.dart';
import 'package:flutter_project/pages/home.dart';
import 'package:flutter_project/pages/login.dart';
import 'package:flutter_project/services/notificationservice.dart';


void main() {
  // WidgetsFlutterBinding.ensureInitialized();
  // NotificationService().initNotification();
  runApp(NoteUp());
}

class NoteUp extends StatelessWidget{
  @override
  Widget build(BuildContext context){
    return const MaterialApp(
      title: 'DiaryNote',
      debugShowCheckedModeBanner: false,
      home: login(),
    );
  }
}