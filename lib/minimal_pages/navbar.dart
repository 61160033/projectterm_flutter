import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_project/minimal_pages/notification.dart';
import 'package:flutter_project/pages/chat_bot.dart';
import 'package:flutter_project/pages/home.dart';
import 'package:flutter_project/minimal_pages/setting.dart';
import 'package:path/path.dart';

class NavBar extends StatelessWidget {
  const NavBar({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            UserAccountsDrawerHeader(
                accountName: Text('Example'),
                accountEmail: Text('Example@gmail.com'),
              currentAccountPicture: CircleAvatar(
                child: ClipOval(
                  child: Image.network('https://cdn-icons-png.flaticon.com/512/149/149071.png',
                  width: 90,
                  height: 90,
                  fit: BoxFit.cover,),
                ),
              ),
              decoration: BoxDecoration(
                color: Colors.blue,
                image: DecorationImage(
                  image: NetworkImage(
                    'https://data.1freewallpapers.com/download/calm-body-of-water-surrounded-with-trees-and-mountains-during-daytime-4k-nature.jpg',
                  ),
                  fit: BoxFit.cover,
                )
              ),
            ),
            ListTile(
              leading: Icon(Icons.home),
              title: Text('Home'),
              onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context) => Home())),
            ),
            ListTile(
              leading: Icon(Icons.favorite),
              title: Text('Important'),
              onTap: () => null,
            ),
            ListTile(
              leading: Icon(Icons.notifications),
              title: Text('Notifications'),
              onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context) => Notifications())),
            ),
            ListTile(
              leading: Icon(Icons.chat),
              title: Text('Chat with Joke'),
              onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context) => chatBot())),
            ),
            ListTile(
              leading: Icon(Icons.settings),
              title: Text('Setting'),
              onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context) => Setting())),
            ),
            ListTile(
              leading: Icon(Icons.exit_to_app),
              title: Text('Exit'),
              onTap: () => showDialog(
                context: context,
                builder: (context) => AlertDialog(
                  title: Text('Are you sure?'),
                  content: Text('Do you want to exit from the app?'),
                  actions: [
                    FlatButton(
                      child: Text('EXIT'),
                      onPressed: (){
                        SystemNavigator.pop();
                      },
                    ),
                    FlatButton(
                      child: Text('CANCLE'),
                      onPressed: (){
                        Navigator.pop(context);
                      },
                    ),
                  ],
                ),
              ),
            ),
          ],
        )
    );
  }

}

