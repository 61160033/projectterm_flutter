import 'package:flutter/material.dart';
import 'package:flutter_project/widgets/loading.dart';

class New extends StatefulWidget {
  const New({Key? key}) : super(key: key);

  @override
  _NewState createState() => _NewState();
}

class _NewState extends State<New> {

  late TextEditingController title, content;
  bool loading = false;

  @override
  void initState(){
    super.initState();
    title = new TextEditingController(text: 'Sample title');
    content = new TextEditingController(text: 'Sample content');

  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[800],
      appBar: AppBar(
        backgroundColor: Colors.grey[850],
        title: Text('New Note'),
        actions: <Widget>[
          IconButton(
              onPressed: (){
                setState(() => loading = true); {
                  save();
                };
              },
              icon: Icon(Icons.save)),
        ],
      ),
      body: loading? Loading() : ListView(
        padding: EdgeInsets.all(13.0),
        children: <Widget>[
          TextField(controller: title,style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold, color: Colors.white),),
          SizedBox(height: 10.0),
          TextField(
            controller: content,
            style: TextStyle(fontSize: 18, color: Colors.white),
            maxLines: 27,
          ),
        ],
      ),
    );
  }
  Future<void> save() async{
    await Future.delayed(Duration(seconds: 2));
    setState(() => loading = false);
  }
  Future<void> delete() async{
    await Future.delayed(Duration(seconds: 2));
    Navigator.pop(context);
  }
}
