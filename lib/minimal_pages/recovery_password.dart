import 'package:flutter/material.dart';
class recoveryPassword extends StatefulWidget {
  const recoveryPassword({Key? key}) : super(key: key);

  @override
  _recoveryPasswordState createState() => _recoveryPasswordState();
}

class _recoveryPasswordState extends State<recoveryPassword> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          constraints: BoxConstraints(
            maxHeight: MediaQuery.of(context).size.height,
            maxWidth: MediaQuery.of(context).size.width,
          ),
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [
                Colors.deepPurpleAccent,
                Colors.blue
              ],
              begin: Alignment.topLeft,
              end: Alignment.centerRight,
            ),

          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(flex: 3, child: Padding(
                padding: const EdgeInsets.symmetric(
                    vertical: 36.0, horizontal: 24.0
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Forgot", style: TextStyle(
                        color: Colors.white,
                        fontSize: 40,
                        fontWeight: FontWeight.w800
                    ),
                    ),
                    SizedBox(
                      height: 10.0,),
                    Text("Password", style: TextStyle(
                        color: Colors.white,
                        fontSize: 40,
                        fontWeight: FontWeight.w800
                    ),
                    ),
                    SizedBox(
                      height: 10.0,),
                    Text("Enter your email for recover your password", style: TextStyle(
                        color: Colors.grey[300],
                        fontSize: 16,
                        fontWeight: FontWeight.w800
                    ),
                    ),
                  ],
                ),
              ),
              ),
              Expanded(
                flex: 5,
                child: Container(
                  width: double.infinity,
                  decoration: BoxDecoration(
                      color: Colors.white.withOpacity(0.7),
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(40),
                        topRight: Radius.circular(40),
                      )
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(24.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        SizedBox(
                          height: 20.0,
                        ),
                        TextField(
                          obscureText: true,
                          keyboardType: TextInputType.emailAddress,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8.0),
                              borderSide: BorderSide.none,
                            ),
                            filled: true,
                            fillColor: Color(0xFFe7edeb),
                            hintText: "enter your email",
                            prefixIcon: Icon(
                              Icons.mail,color: Colors.grey[600],
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 50.0,
                        ),
                        Column(
                          children: [
                            Container(
                              width: double.infinity,
                              height: 60,
                              decoration: BoxDecoration(
                                color: Colors.black ,borderRadius: BorderRadius.circular(8),
                              ),
                              child: FlatButton(
                                  onPressed: () {
                                  },
                                  child: Text('Recovery Password', style: TextStyle(color: Colors.white, fontSize: 18),)),
                            )
                          ],
                        )

                      ],
                    ),
                  ),
                ),

              ),
            ],
          ),
        ),
      ),
    );
  }
}
