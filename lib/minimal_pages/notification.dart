import 'package:flutter/material.dart';
import 'package:path/path.dart';

class Notifications extends StatefulWidget {
  const Notifications({Key? key}) : super(key: key);

  @override
  _NotificationsState createState() => _NotificationsState();
}

class _NotificationsState extends State<Notifications> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[400],
      appBar: AppBar(
        title: Text('Notifications', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
        centerTitle: true,
        backgroundColor: Colors.grey[850],

      ),
      body: ListView.separated(
        physics: ClampingScrollPhysics(),
        padding: EdgeInsets.zero,
        itemCount: 10,
        itemBuilder: (context, index){
          return ListTile(
            title: Text('Example',style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold)),
            subtitle: Text('You have a plan on 11/05/2021', style: TextStyle(color: Colors.black)),
            onTap: (){},
            enabled: true,
          );
        },separatorBuilder: (context, index){
          return Divider(height: 20, thickness: 1);
      },
          )
    );
  }
}
