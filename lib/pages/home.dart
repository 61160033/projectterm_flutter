import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_project/minimal_pages/edit.dart';
import 'package:flutter_project/pages/chat_bot.dart';
import 'package:flutter_project/minimal_pages/navbar.dart';
import 'package:flutter_project/minimal_pages/new.dart';
import 'package:flutter_project/minimal_pages/setting.dart';
import 'package:flutter_project/widgets/loading.dart';

import 'login.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {

  bool loading = true;
  Icon cusIcon = Icon(Icons.search);
  Widget cusSearchBar = Text("Diary Notes");

  @override
  void initState(){
    super.initState();
    refresh();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: NavBar(),
      backgroundColor: Colors.grey[800],
      appBar: AppBar(
        backgroundColor: Colors.grey[850],
        title: cusSearchBar,
        actions: <Widget>[
          IconButton(
            onPressed: (){
              setState(() {
                var a = 1;
                if(a == 1){
                  this.cusIcon = Icon(Icons.cancel);
                  this.cusSearchBar = TextField(
                    textInputAction: TextInputAction.go,
                    decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: "Search Note",
                      hintStyle: TextStyle(fontSize: 16.0, color: Colors.white)
                    ),
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 16.0,
                    ),
                  ); a = 2;
                }
                else{
                  this.cusIcon = Icon(Icons.search);
                  this.cusSearchBar = Text("Diary Notes");
                  a = 1;
                }
              });
              },
            icon: cusIcon,
          ),
          // IconButton(
          //   icon: Icon(
          //     Icons.notifications,
          //     color: Colors.white,
          //   ),
          //   onPressed: () {
          //     Navigator.push(context, MaterialPageRoute(builder: (context) => chatBot()));
          //   },
          // ),
          Padding(
              padding: EdgeInsets.only(right: 10.0),
              child: PopupMenuButton(
                itemBuilder: (context) =>[
                  PopupMenuItem(
                    value: 0,
                    child: ListTile(
                      leading: Icon(Icons.settings,
                        color: Colors.black45,
                      ),
                      title: Text('Setting'),
                    ),
                  ),
                  PopupMenuItem(
                    value: 1,
                    child: ListTile(
                      leading: Icon(Icons.logout,
                        color: Colors.black45,
                      ),
                      title: Text('Logout'),
                    ),
                  ),
                ],
                child: Icon(
                  Icons.more_vert,
                  size: 28.0,
                ),
                onSelected: (result) {
                  if (result == 0) {
                    Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(builder: (context) => Setting()),
                    );
                  }
                  else if(result == 1){
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => login()),
                    );
                  }
                },
              ))
        ],
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: (){
          setState(() => loading = true);
          Navigator.push(context, MaterialPageRoute(builder: (context)=> New())).then((v) {
            refresh();
          });
        },
      ),
      body: loading? Loading(): ListView.builder(
        padding: EdgeInsets.all(7.0),
        itemCount: 10,
          itemBuilder: (context, index){
          return Container(
            margin: EdgeInsets.all(15),
            padding: EdgeInsets.all(15),
            height: 350,
            decoration: BoxDecoration(
              color: Colors.grey[600], borderRadius: BorderRadius.circular(15)
            ),
            child: ListTile(
              title: Text('Title' + index.toString(),
                  style: TextStyle(
                      fontSize: 22,
                      fontWeight: FontWeight.bold,
                      color: Colors.white
              )),
              subtitle: Text('Sample content',style: TextStyle(fontSize: 18, color: Colors.white),),
              onTap: (){
                setState(() => loading = true);
                Navigator.push(context, MaterialPageRoute(builder: (context)=> Edit())).then((v) {
                  refresh();
                });
              },
            ),
          );
          }),
    );
  }

  Future<void> refresh() async {
    await Future.delayed(Duration(seconds: 1));
    setState(() => loading = false);
  }

}
