
import 'package:flutter/material.dart';
import 'package:flutter_project/minimal_pages/edit.dart';
import 'package:flutter_project/pages/home.dart';
import 'package:flutter_project/pages/login.dart';
import 'package:flutter_project/minimal_pages/navbar.dart';
import 'package:flutter_project/minimal_pages/new.dart';
import 'package:flutter_project/minimal_pages/setting.dart';
import 'package:flutter_project/services/notificationservice.dart';
import 'package:flutter_project/widgets/loading.dart';
import 'package:intl/intl.dart';
import 'package:timezone/timezone.dart' as tz;
import 'package:timezone/data/latest.dart' as tz;

class chatBot extends StatefulWidget {
  const chatBot({Key? key}) : super(key: key);

  @override
  _chatBotState createState() => _chatBotState();

}


class _chatBotState extends State<chatBot> {
  // @override
  // void initState(){
  //   super.initState();
  //   tz.initializeTimeZones();
  // }
  final messageController = TextEditingController();



  @override
  Widget build(BuildContext context) {
    DateTime now = DateTime.now();
    String formattedDate = DateFormat('kk:mm').format(now);
    return Scaffold(
      backgroundColor: Colors.grey[800],
      drawer: NavBar(),
      appBar: AppBar(
        title: Text("Joke W The Assistant"),
        backgroundColor: Colors.grey[850],
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.notifications,
              color: Colors.white,
            ),
            onPressed: () {
              // NotificationService().showNotification(1, 'Example', 'about the notification', 5);
              Navigator.push(context, MaterialPageRoute(builder: (context) => Home()));
            },
          ),
          Padding(
              padding: EdgeInsets.only(right: 10.0),
              child: PopupMenuButton(
                  itemBuilder: (context) =>[
                    PopupMenuItem(
                      value: 0,
                      child: ListTile(
                        leading: Icon(Icons.settings,
                        color: Colors.black45,
                        ),
                        title: Text('Setting'),
                      ),
                    ),
                    PopupMenuItem(
                      value: 0,
                      child: ListTile(
                        leading: Icon(Icons.logout,
                          color: Colors.black45,
                        ),
                        title: Text('Logout'),
                      ),
                    ),
                  ],
                  child: Icon(
                    Icons.more_vert,
                    size: 28.0,
          ),
                onSelected: (result) {
                  if (result == 0) {
                    Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=> Setting()));
                  }
                  else if(result == 1){
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => login()),
                    );
                  }
                },
          ))
        ],
      ),


      body: Container(
        child: Column(
          children: [
            Center(
              child: Container(
                padding: EdgeInsets.only(top: 15, bottom: 10),
                child: Text("Today, " +formattedDate,style: TextStyle(
                  color: Colors.white,
                    fontSize: 18
                ),
                ),
              ),
            ),

            Flexible(
              child: ListView.builder(
                reverse: true,
                  itemCount: 0,
                  itemBuilder: (context, index){
                    return Container(

                    );
                  }),
            ),


            Divider(
              height: 5,
              color: Colors.greenAccent,
            ),
            Container(
              child: ListTile(
                leading: IconButton(
                  icon: Icon(Icons.camera_alt, color: Colors.greenAccent, size: 30,), onPressed: () {  },
                ),
                title: Container(
                  height: 35,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(15)),
                    color: Color.fromRGBO(220, 220, 220, 1)
                  ),
                  padding: EdgeInsets.only(left: 15),
                  child: TextFormField(
                    controller: messageController,
                    decoration: InputDecoration(
                      hintText: "Enter a message",
                      hintStyle: TextStyle(
                        color: Colors.black26
                      ),
                      border: InputBorder.none,
                      focusedBorder: InputBorder.none,
                      enabledBorder: InputBorder.none,
                      errorBorder: InputBorder.none,
                      disabledBorder: InputBorder.none,

                    ),
                    style: TextStyle(
                      fontSize: 16,
                      color: Colors.black
                    ),
                  ),
                ),
                trailing: IconButton(
                  icon: Icon(Icons.send,
                  size: 30,
                  color: Colors.greenAccent,)
                  , onPressed: () {
                    if(messageController.text.isEmpty){
                      print("empty message");
                    }
                    else{
                      setState(() {

                      });
                    }
                },
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
